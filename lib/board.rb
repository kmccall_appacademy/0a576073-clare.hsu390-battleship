
class Board
  attr_accessor :grid
  def initialize(grid=Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10).map! {Array.new(10)}
  end

  def [](idx)
    @grid[idx[0]][idx[1]]
  end

  def []=(idx, value)
    @grid[idx] = value
  end

  def count
    @grid.flatten.count(:s)
  end

  def empty?(pos="none")
    if pos == "none"
      @grid.flatten.all? {|el| el.nil?}
    else
      x,y = pos
      @grid[x][y].nil?
    end
  end

  def full?
    @grid.flatten.all? {|el| !el.nil?}
  end

  def place_random_ship
    if full?
      raise_error
    else
      x_arr = (0...@grid.length).to_a
      y_arr = (0...@grid[0].length).to_a
        x = x_arr.sample
        y = y_arr.sample
        @grid[x][y] = :s if @grid[x][y].nil?
    end
  end

  def won?
    return false if @grid.flatten.any? {|el| el == :s}
    true
  end

end
