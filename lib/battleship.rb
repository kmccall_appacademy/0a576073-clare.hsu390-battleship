class BattleshipGame
  attr_reader :board, :player
  def initialize(player, board)
    @player = player
    @board = board
  end

  def attack(arr)
    @board.grid[arr[0]][arr[1]] = :x
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    pos = @player.get_play
    self.attack(pos)
  end


end
